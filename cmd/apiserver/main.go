package main

import (
	"bitbucket.org/cam57/back_guestbook/internal/app/apiserver"
	"flag"
	"github.com/BurntSushi/toml"
	"log"
	"os"
)

var (
	configPath string
)

func init() {

	_, existAppEnv := os.LookupEnv("APP_ENV")
	if !existAppEnv {
		pathConfig := "config/apiserver.toml"

		flag.StringVar(&configPath, "config-path", pathConfig, "path to config file")
	}
}

func main() {
	_, existAppEnv := os.LookupEnv("APP_ENV")
	if !existAppEnv {
		flag.Parse()
	}

	config := apiserver.NewConfig()

	if !existAppEnv {
		_, err := toml.DecodeFile(configPath, config)
		if err != nil {
			log.Fatal(err)
		}
	}

	if err := apiserver.Start(config); err != nil {
		log.Fatal(err)
	}
}
