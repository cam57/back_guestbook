module bitbucket.org/cam57/back_guestbook

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/asaskevich/govalidator v0.0.0-20200108200545-475eaeb16496
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/google/uuid v1.1.1
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/lib/pq v1.3.0
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/crypto v0.0.0-20200221231518-2aa609cf4a9d
)
