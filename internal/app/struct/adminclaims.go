package _struct

import "github.com/dgrijalva/jwt-go"

type AdminClaimsType struct {
	jwt.StandardClaims
	Id       int    `json:"id"`
	Role     string `json:"role"`
	FullName string `json:"full_name"`
}
