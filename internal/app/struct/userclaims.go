package _struct

import "github.com/dgrijalva/jwt-go"

type UserClaimsType struct {
	jwt.StandardClaims
	Id int `json:"id"`
}
