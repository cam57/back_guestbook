package respond

import (
	"encoding/json"
	"net/http"
)

func Error(w http.ResponseWriter, r *http.Request, code int, err error) {
	Respond(w, r, code, map[string]string{"error": err.Error()})
}

func Respond(w http.ResponseWriter, r *http.Request, code int, data interface{}) {
	w.WriteHeader(code)
	if data != nil {
		if err := json.NewEncoder(w).Encode(data); err != nil {
			json.NewEncoder(w).Encode(map[string]string{"error": err.Error()})
		}
	}
}

func ValidateErrors(w http.ResponseWriter, r *http.Request, code int, data []byte) {
	w.WriteHeader(code)
	if _, err := w.Write(data); err != nil {
		Respond(w, r, http.StatusInternalServerError, err)
	}
}
