package model

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"regexp"
)

type PhoneCode struct {
	ID          int    `json:"id"`
	Phone       string `json:"phone"`
	Code        int    `json:"code"`
	ExpiredDate string `json:"-"`
}

// Validate ...
func (pc *PhoneCode) Validate() error {
	return validation.ValidateStruct(
		pc,
		validation.Field(&pc.Phone, validation.Match(regexp.MustCompile("^((7){1}([0-9]){10})$")).Error("Неверный формат номера"), validation.Length(11, 16)),
	)
}

func (pc *PhoneCode) Sanitize() {
	pc.Code = 0
}
