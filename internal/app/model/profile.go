package model

type Profile struct {
	Id         int    `json:"id"`
	Inn        string `json:"inn"`
	AlterTitle string `json:"alter_title"`
}
