package model

import (
	"github.com/asaskevich/govalidator"
	"github.com/go-ozzo/ozzo-validation"
	"golang.org/x/crypto/bcrypt"
	"regexp"
)

type User struct {
	ID             int    `json:"id"`
	Email          string `json:"email"`
	Password       string `json:"password"`
	Phone          string `json:"phone"`
	FullName       string `json:"full_name"`
	EmailConfirmed string `json:"email_confirmed"`
	ProfileId      int    `json:"profile_id"`
	Role           string `json:"role"`
}

//BeforeCreate ...
func (u *User) BeforeCreate() error {
	if len(u.Password) > 0 {
		enc, err := encryptString(u.Password)
		if err != nil {
			return err
		}

		u.Password = enc
	}

	return nil
}

//BeforeUpdate ...
func (u *User) BeforeUpdate() error {
	if len(u.Password) > 0 {
		enc, err := encryptString(u.Password)
		if err != nil {
			return err
		}

		u.Password = enc
	}

	return nil
}

func encryptString(s string) (string, error) {
	b, err := bcrypt.GenerateFromPassword([]byte(s), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}

	return string(b), nil
}

// Validate ...
func (u *User) Validate() error {
	return validation.ValidateStruct(
		u,
		validation.Field(&u.Email, validation.Required.Error("Поле обязательно для заполнения"), validation.NewStringRule(govalidator.IsEmail, "Неверный формат почты")),
		validation.Field(&u.Password, validation.Match(regexp.MustCompile("^[a-zA-Z0-9]{8,}$")).Error("Используйте цифры и заглавные буквы.  Минимальная длина - 8 символов. Знаки  (пробел) ! \" # $ % & ' ( ) * + , - . / : ; < = > ? @ [ \\ ] ^ _` { | } ~ - НЕ используются в пароле.")),
		validation.Field(&u.FullName, validation.Required.Error("Обязательно для заполнения"), validation.RuneLength(3, 40).Error("Длина ФИО должна быть от 3 до 40 символов")),
		validation.Field(&u.Phone, validation.Match(regexp.MustCompile("^((7){1}([0-9]){10})$")).Error("Неверный формат телефона"), validation.Length(11, 16).Error("Длина телефона должна быть от 11 до 16 символов")),
	)
}

// Validate ...
func (u *User) ValidateUpdate() error {
	return validation.ValidateStruct(
		u,
		validation.Field(&u.FullName, validation.Required.Error("Обязательно для заполнения"), validation.RuneLength(3, 40).Error("Длина ФИО должна быть от 3 до 40 символов")),
		validation.Field(&u.Password, validation.Match(regexp.MustCompile("^[a-zA-Z0-9]{8,}$")).Error("Используйте цифры и заглавные буквы.  Минимальная длина - 8 символов. Знаки  (пробел) ! \" # $ % & ' ( ) * + , - . / : ; < = > ? @ [ \\ ] ^ _` { | } ~ - НЕ используются в пароле.Используйте цифры и заглавные буквы.  Минимальная длина - 8 символов. Знаки  (пробел) ! \" # $ % & ' ( ) * + , - . / : ; < = > ? @ [ \\ ] ^ _` { | } ~ - НЕ используются в пароле.")),
	)
}

func (u *User) Sanitize() {
	u.Password = ""
}

func (u *User) ComparePassword(password string) bool {
	return bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password)) == nil
}
