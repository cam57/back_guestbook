package model

import (
	"github.com/asaskevich/govalidator"
	"github.com/go-ozzo/ozzo-validation"
	"golang.org/x/crypto/bcrypt"
	"regexp"
)

type AdminUser struct {
	ID       int    `json:"id"`
	Email    string `json:"email"`
	Password string `json:"-"`
	FullName string `json:"full_name"`
	Role     string `json:"role"`
}

//BeforeCreate ...
func (u *AdminUser) BeforeCreate() error {
	if len(u.Password) > 0 {
		enc, err := encryptString(u.Password)
		if err != nil {
			return err
		}

		u.Password = enc
	}

	return nil
}

//BeforeUpdate ...
func (u *AdminUser) AdminUser() error {
	if len(u.Password) > 0 {
		enc, err := encryptString(u.Password)
		if err != nil {
			return err
		}

		u.Password = enc
	}

	return nil
}

// Validate ...
func (u *AdminUser) Validate() error {
	return validation.ValidateStruct(
		u,
		validation.Field(&u.Email, validation.Required.Error("Поле обязательно для заполнения"), validation.NewStringRule(govalidator.IsEmail, "Неверный формат почты")),
		validation.Field(&u.Password, validation.Match(regexp.MustCompile("^[a-zA-Z0-9]{8,}$")).Error("Неверный формат пароля")),
		validation.Field(&u.FullName, validation.Required.Error("Обязательно для заполнения"), validation.RuneLength(3, 40).Error("Длина пароля должна быть от 3 до 40 символов")),
	)
}

func (u *AdminUser) Sanitize() {
	u.Password = ""
}

func (u *AdminUser) ComparePassword(password string) bool {
	return bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password)) == nil
}
