package constant

import "errors"

var (
	ErrIncorrectEmailOrPassword = errors.New("Неверный пароль или почта")
	ErrIncorrectConfirmPassword = errors.New("Пароль и подтверждение пароля не совпадают")
	ErrIncorrectOldPassword     = errors.New("Старый пароль не совпадает")
	ErrIncorrectCodePhone       = errors.New("Неверный код подтверждения телефона")
	ErrEmailExist               = errors.New("Почта уже используется")
	ErrPhoneExist               = errors.New("Телефон уже используется")
	ErrAccessDenied             = errors.New("Операция запрещена")
	ErrInvalidDatePeriod        = errors.New("Выбран неверный период дат")
	ErrInvalidIds               = errors.New("Неверный формат параметров")
	ErrPhoneNotFound            = errors.New("Пользователь не найден")
)
