package constant

const (
	FormatDate       string = "2006-01-02 15:04:05"
	ProductionAppEnv string = "production"
	Yes              string = "Y"
	No               string = "N"
	BigBoss          string = "big_boss"
	Admin            string = "admin"
)
