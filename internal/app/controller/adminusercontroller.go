package controller

import (
	"bitbucket.org/cam57/back_guestbook/internal/app/constant"
	"bitbucket.org/cam57/back_guestbook/internal/app/model"
	"bitbucket.org/cam57/back_guestbook/internal/app/respond"
	"bitbucket.org/cam57/back_guestbook/internal/app/store"
	"bitbucket.org/cam57/back_guestbook/internal/app/struct"
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"os"
	"time"
)

func HandleCreateAdminUser(store store.Store) http.HandlerFunc {

	type requestBody struct {
		FullName        string `json:"full_name"`
		Password        string `json:"password"`
		ConfirmPassword string `json:"confirm_password"`
		Email           string `json:"email"`
	}

	return func(writer http.ResponseWriter, request *http.Request) {

		req := &requestBody{}
		if err := json.NewDecoder(request.Body).Decode(req); err != nil {
			respond.Error(writer, request, http.StatusBadRequest, err)
			return
		}

		if req.Password != req.ConfirmPassword {
			respond.Error(writer, request, http.StatusUnprocessableEntity, constant.ErrIncorrectConfirmPassword)
			return
		}

		u := &model.AdminUser{
			FullName: req.FullName,
			Password: req.Password,
			Email:    req.Email,
			Role:     constant.BigBoss,
		}

		if err := store.AdminUser().Create(u); err != nil {
			validateErrors, jsonErr := json.Marshal(err)
			if jsonErr == nil {
				respond.ValidateErrors(writer, request, http.StatusUnprocessableEntity, validateErrors)
				return
			}

			respond.Error(writer, request, http.StatusUnprocessableEntity, err)
			return
		}

		u.Sanitize()

		respond.Respond(writer, request, http.StatusCreated, u)
	}
}

func HandleAdminSignIn(store store.Store) http.HandlerFunc {

	type requestBody struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}

	type response struct {
		Id       int    `json:"id"`
		Expired  int64  `json:"expired"`
		FullName string `json:"full_name"`
		Email    string `json:"email"`
		Token    string `json:"token"`
	}

	return func(writer http.ResponseWriter, request *http.Request) {

		req := &requestBody{}
		if err := json.NewDecoder(request.Body).Decode(req); err != nil {
			respond.Error(writer, request, http.StatusBadRequest, err)
			return
		}

		u, err := store.AdminUser().FindByEmail(req.Email)
		if err != nil {
			respond.Error(writer, request, http.StatusUnauthorized, err)
			return
		}

		if !u.ComparePassword(req.Password) {
			respond.Error(writer, request, http.StatusUnauthorized, constant.ErrIncorrectEmailOrPassword)
			return
		}

		var claims = _struct.AdminClaimsType{
			jwt.StandardClaims{
				Audience:  "",
				ExpiresAt: time.Now().Add(time.Hour * 24 * 7).Unix(),
				Id:        "",
				IssuedAt:  0,
				Issuer:    "test",
				NotBefore: 0,
				Subject:   "",
			},
			u.ID,
			u.Role,
			u.FullName,
		}

		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

		secretKey := "secret key"
		if secretKeyEnv, existSecret := os.LookupEnv("ADMIN_SECRET_KEY"); existSecret {
			secretKey = secretKeyEnv
		}

		var mySigningKey = []byte(secretKey)

		// Подписываем токен нашим секретным ключем
		tokenString, err := token.SignedString(mySigningKey)

		if err != nil {
			respond.Error(writer, request, http.StatusUnprocessableEntity, err)
			return
		}
		res := response{
			Token:    tokenString,
			Id:       u.ID,
			FullName: u.FullName,
			Email:    u.Email,
			Expired:  time.Now().Add(time.Hour * 24 * 7).Unix(),
		}

		respond.Respond(writer, request, http.StatusOK, res)
	}
}
