package controller

import (
	"bitbucket.org/cam57/back_guestbook/internal/app/constant"
	"bitbucket.org/cam57/back_guestbook/internal/app/model"
	"bitbucket.org/cam57/back_guestbook/internal/app/respond"
	"bitbucket.org/cam57/back_guestbook/internal/app/store"
	st "bitbucket.org/cam57/back_guestbook/internal/app/store"
	"bitbucket.org/cam57/back_guestbook/internal/app/struct"
	"bitbucket.org/cam57/back_guestbook/internal/app/task"
	"encoding/json"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"time"
)

func HandleSignUp(store store.Store) http.HandlerFunc {
	type requestSignUP struct {
		Inn             string `json:"inn"`
		Email           string `json:"email"`
		Password        string `json:"password"`
		ConfirmPassword string `json:"confirm_password"`
		Phone           string `json:"phone"`
		FullName        string `json:"full_name"`
		Code            int    `json:"code"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		req := &requestSignUP{}

		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			respond.Error(w, r, http.StatusBadRequest, err)
			return
		}

		if req.Password != req.ConfirmPassword {
			respond.Error(w, r, http.StatusUnprocessableEntity, constant.ErrIncorrectConfirmPassword)
			return
		}

		if _, err := store.User().FindByEmail(req.Email); err == nil {
			respond.Error(w, r, http.StatusUnprocessableEntity, constant.ErrEmailExist)
			return
		}

		if _, err := store.User().FindByPhone(req.Phone); err == nil {
			respond.Error(w, r, http.StatusUnprocessableEntity, constant.ErrPhoneExist)
			return
		}

		nowDate := time.Now()

		if _, err := store.PhoneCode().FindByPhoneAndCode(req.Phone, req.Code, nowDate); err != nil {
			respond.Error(w, r, http.StatusUnprocessableEntity, constant.ErrIncorrectCodePhone)
			return
		}

		profile := &model.Profile{
			Inn: req.Inn,
		}

		if err := store.Profile().Create(profile); err != nil {
			validateErrors, jsonErr := json.Marshal(err)
			if jsonErr == nil {
				respond.ValidateErrors(w, r, http.StatusUnprocessableEntity, validateErrors)
				return
			}

			respond.Error(w, r, http.StatusUnprocessableEntity, err)
			return
		}

		u := &model.User{
			Email:     req.Email,
			Password:  req.Password,
			Phone:     req.Phone,
			FullName:  req.FullName,
			ProfileId: profile.Id,
			Role:      constant.Admin,
		}

		if err := store.User().Create(u); err != nil {
			validateErrors, jsonErr := json.Marshal(err)
			if jsonErr == nil {
				respond.ValidateErrors(w, r, http.StatusUnprocessableEntity, validateErrors)
				return
			}

			respond.Error(w, r, http.StatusUnprocessableEntity, err)
			return
		}

		u.Sanitize()

		go task.Send(u, store)

		respond.Respond(w, r, http.StatusCreated, u)
	}
}

func HandleSignIn(store store.Store) http.HandlerFunc {
	type request struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}

	type response struct {
		Id             int    `json:"id"`
		Expired        int64  `json:"expired"`
		FullName       string `json:"full_name"`
		Phone          string `json:"phone"`
		Email          string `json:"email"`
		Token          string `json:"token"`
		EmailConfirmed string `json:"email_confirmed"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		req := &request{}
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			respond.Error(w, r, http.StatusBadRequest, err)
			return
		}

		u, err := store.User().FindByEmail(req.Email)
		if err != nil {
			respond.Error(w, r, http.StatusUnauthorized, err)
			return
		}

		if !u.ComparePassword(req.Password) {
			respond.Error(w, r, http.StatusUnauthorized, constant.ErrIncorrectEmailOrPassword)
			return
		}

		var claims = _struct.UserClaimsType{
			jwt.StandardClaims{
				Audience:  "",
				ExpiresAt: time.Now().Add(time.Hour * 24 * 7).Unix(),
				Id:        "",
				IssuedAt:  0,
				Issuer:    "test",
				NotBefore: 0,
				Subject:   "",
			},
			u.ID,
		}

		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

		secretKey := "secret key"
		if secretKeyEnv, existSecret := os.LookupEnv("SECRET_KEY"); existSecret {
			secretKey = secretKeyEnv
		}

		var mySigningKey = []byte(secretKey)

		// Подписываем токен нашим секретным ключем
		tokenString, err := token.SignedString(mySigningKey)

		if err != nil {
			respond.Error(w, r, http.StatusUnprocessableEntity, err)
			return
		}
		res := response{
			Token:          tokenString,
			Id:             u.ID,
			FullName:       u.FullName,
			Email:          u.Email,
			Phone:          u.Phone,
			Expired:        time.Now().Add(time.Hour * 24 * 7).Unix(),
			EmailConfirmed: u.EmailConfirmed,
		}

		respond.Respond(w, r, http.StatusOK, res)
	}
}

func HandleSendCode(store store.Store, registration bool) http.HandlerFunc {
	type request struct {
		Phone string `json:"phone"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		req := &request{}
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			respond.Error(w, r, http.StatusBadRequest, err)
			return
		}

		if _, err := store.User().FindByPhone(req.Phone); err == nil && registration {
			respond.Error(w, r, http.StatusUnprocessableEntity, constant.ErrPhoneExist)
			return
		} else if err == st.ErrRecordNotFound && !registration {
			respond.Error(w, r, http.StatusUnprocessableEntity, constant.ErrPhoneNotFound)
			return
		}

		nowDate := time.Now().Add(time.Minute * 2)

		if exist, err := store.PhoneCode().FindByPhoneAndDate(req.Phone, nowDate); err != nil {
			respond.Error(w, r, http.StatusInternalServerError, err)
			return
		} else if exist {
			respond.Error(w, r, http.StatusInternalServerError, errors.New("Смс-код можно отправлять раз в две минуты"))
			return
		}

		phoneCode := &model.PhoneCode{}
		phoneCode.ExpiredDate = nowDate.Add(time.Minute * 2).Format(constant.FormatDate)

		appEnv, existAppEnv := os.LookupEnv("APP_ENV")

		if existAppEnv && appEnv == constant.ProductionAppEnv {
			phoneCode.Phone = req.Phone
			phoneCode.Code = rand.Intn(8999) + 1000

			if err := store.PhoneCode().Create(phoneCode); err != nil {
				respond.Error(w, r, http.StatusUnprocessableEntity, err)
				return
			}

			request, err := http.NewRequest("GET", "https://smsc.ru/sys/send.php?login=wersia&psw=Mirvam2015&phones="+phoneCode.Phone+"&mes=Код подтверждения для регистрации: "+strconv.Itoa(phoneCode.Code)+"&sender=tarecon&charset=utf-8&fmt=3", nil)
			if err != nil {
				respond.Error(w, r, http.StatusUnprocessableEntity, err)
				return
			}

			client := &http.Client{}
			if _, err := client.Do(request); err != nil {
				respond.Error(w, r, http.StatusUnprocessableEntity, err)
				return
			}
		} else {
			phoneCode.Phone = req.Phone
			phoneCode.Code = 1234

			if err := store.PhoneCode().Create(phoneCode); err != nil {
				respond.Error(w, r, http.StatusUnprocessableEntity, err)
				return
			}
		}

		phoneCode.Sanitize()
		respond.Respond(w, r, http.StatusOK, phoneCode)
	}
}

func HandleUpdateUser(store store.Store) http.HandlerFunc {

	type requestBody struct {
		FullName    string `json:"full_name"`
		NewPassword string `json:"new_password"`
		OldPassword string `json:"old_password"`
	}

	return func(writer http.ResponseWriter, request *http.Request) {
		user := request.Context().Value("user").(*model.User)

		req := &requestBody{}
		if err := json.NewDecoder(request.Body).Decode(req); err != nil {
			respond.Error(writer, request, http.StatusBadRequest, err)
			return
		}

		if !user.ComparePassword(req.OldPassword) && len(req.NewPassword) > 0 {
			respond.Error(writer, request, http.StatusUnprocessableEntity, constant.ErrIncorrectOldPassword)
			return
		}

		user.Password = req.NewPassword
		user.FullName = req.FullName

		if err := store.User().Update(user); err != nil {
			validateErrors, jsonErr := json.Marshal(err)
			if jsonErr == nil {
				respond.ValidateErrors(writer, request, http.StatusUnprocessableEntity, validateErrors)
				return
			}

			respond.Error(writer, request, http.StatusUnprocessableEntity, err)
			return
		}

		user.Sanitize()

		respond.Respond(writer, request, http.StatusOK, user)
	}
}

func HandleRecoverPassword(store store.Store) http.HandlerFunc {
	type request struct {
		Phone           string `json:"phone"`
		Password        string `json:"password"`
		ConfirmPassword string `json:"confirm_password"`
		Code            int    `json:"code"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		req := &request{}
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			respond.Error(w, r, http.StatusBadRequest, err)
			return
		}

		if req.Password != req.ConfirmPassword {
			respond.Error(w, r, http.StatusUnprocessableEntity, constant.ErrIncorrectConfirmPassword)
			return
		}

		nowDate := time.Now()

		if _, err := store.PhoneCode().FindByPhoneAndCode(req.Phone, req.Code, nowDate); err != nil {
			respond.Error(w, r, http.StatusUnprocessableEntity, constant.ErrIncorrectCodePhone)
			return
		}

		user, err := store.User().FindByPhone(req.Phone)
		if err != nil {
			respond.Error(w, r, http.StatusUnprocessableEntity, err)
		}

		user.Password = req.Password

		if err := store.User().Update(user); err != nil {
			validateErrors, jsonErr := json.Marshal(err)
			if jsonErr == nil {
				respond.ValidateErrors(w, r, http.StatusUnprocessableEntity, validateErrors)
				return
			}

			respond.Error(w, r, http.StatusUnprocessableEntity, err)
			return
		}

		user.Sanitize()

		respond.Respond(w, r, http.StatusOK, user)
	}
}

func HandleEqualPhoneCode(store store.Store) http.HandlerFunc {
	type request struct {
		Phone string `json:"phone"`
		Code  int    `json:"code"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		req := &request{}
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			respond.Error(w, r, http.StatusBadRequest, err)
			return
		}

		nowDate := time.Now()

		phoneCode, err := store.PhoneCode().FindByPhoneAndCode(req.Phone, req.Code, nowDate)
		if err != nil {
			respond.Error(w, r, http.StatusUnprocessableEntity, constant.ErrIncorrectCodePhone)
			return
		}

		phoneCode.Sanitize()

		respond.Respond(w, r, http.StatusOK, phoneCode)
	}
}

func HandleEmailConfirmation(store store.Store) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		vars := mux.Vars(r)

		ec, err := store.EmailConfirmation().FindByToken(vars["token"])
		if err != nil {
			respond.Error(w, r, http.StatusBadRequest, err)
			return
		}

		if err = store.User().EmailConfirmation(ec.UserId); err != nil {
			respond.Error(w, r, http.StatusBadRequest, err)
			return
		}

		respond.Respond(w, r, http.StatusOK, map[string]string{"status": "ok"})
	}
}
