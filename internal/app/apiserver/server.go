package apiserver

import (
	"bitbucket.org/cam57/back_guestbook/internal/app/controller"
	"bitbucket.org/cam57/back_guestbook/internal/app/respond"
	"bitbucket.org/cam57/back_guestbook/internal/app/store"
	"bitbucket.org/cam57/back_guestbook/internal/app/struct"
	"context"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

type Server struct {
	Router *mux.Router
	logger *logrus.Logger
	Store  store.Store
}

func newServer(store store.Store) *Server {

	s := &Server{
		Router: mux.NewRouter(),
		logger: logrus.New(),
		Store:  store,
	}

	_, existAppEnv := os.LookupEnv("APP_ENV")
	if !existAppEnv {
		outfile, _ := os.OpenFile("logs/application.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		s.logger.SetOutput(outfile)
	}

	s.configureRouter()

	return s
}

const (
	ctxKeyRequestId ctxKey = iota
)

type ctxKey int8

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.Router.ServeHTTP(w, r)
}

func (s *Server) configureRouter() {

	s.logger.Println("server RUN!!!!")
	s.Router.Use(s.setRequestId)
	s.Router.Use(s.logRequest)
	s.Router.Use(setContentTypeMiddleware)
	s.Router.Use(handlers.CORS(handlers.AllowedOrigins([]string{"*"}), handlers.AllowedMethods([]string{"GET", "POST", "OPTIONS", "DELETE", "PUT"}), handlers.AllowedHeaders([]string{"Authorization"})))

	s.Router.HandleFunc("/sign_up", controller.HandleSignUp(s.Store)).Methods(http.MethodPost)
	s.Router.HandleFunc("/sign_in", controller.HandleSignIn(s.Store)).Methods(http.MethodPost)
	s.Router.HandleFunc("/send_code", controller.HandleSendCode(s.Store, true)).Methods(http.MethodPost)
	s.Router.HandleFunc("/send_code/recover_password", controller.HandleSendCode(s.Store, false)).Methods(http.MethodPost)
	s.Router.HandleFunc("/recover_password", controller.HandleRecoverPassword(s.Store)).Methods(http.MethodPost)
	s.Router.HandleFunc("/equal_phone_code", controller.HandleEqualPhoneCode(s.Store)).Methods(http.MethodPost)
	s.Router.HandleFunc("/email_confirmation", controller.HandleEmailConfirmation(s.Store)).Methods(http.MethodGet)

	s.Router.HandleFunc("/admin", controller.HandleCreateAdminUser(s.Store)).Methods(http.MethodPost)
	s.Router.HandleFunc("/admin/sign_in", controller.HandleAdminSignIn(s.Store)).Methods(http.MethodPost)

	private := s.Router.PathPrefix("").Subrouter()
	private.Use(s.authorizationMiddleware)
	private.Use(s.getLimit)

	private.HandleFunc("/users", controller.HandleUpdateUser(s.Store)).Methods(http.MethodPost)

	admin := s.Router.PathPrefix("/admin").Subrouter()
	admin.Use(s.adminAuthorizationMiddleware)
	admin.Use(s.getLimit)
}

func setContentTypeMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}

func (s *Server) authorizationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		tokenString := ""
		headerValue := r.Header.Get("Authorization")

		if strings.HasPrefix(headerValue, "Bearer ") && headerValue != "" {
			tokenString = strings.TrimPrefix(headerValue, "Bearer ")
		} else {
			respond.Error(w, r, http.StatusUnauthorized, errors.New("Ошибка авторизации"))
			return
		}

		token, err := jwt.ParseWithClaims(tokenString, &_struct.UserClaimsType{}, func(token *jwt.Token) (interface{}, error) {
			alg := token.Header["alg"]
			if alg != "HS256" {
				return nil, errors.New("Ошибка авторизации")
			}

			if secretKey, existSecret := os.LookupEnv("SECRET_KEY"); existSecret {
				return []byte(secretKey), nil
			} else {
				return []byte("secret key"), nil
			}
		})

		if err != nil {
			respond.Error(w, r, http.StatusUnauthorized, errors.New("Ошибка авторизации"))
			return
		}

		claims, ok := token.Claims.(*_struct.UserClaimsType)

		if ok && token.Valid {
			exp := claims.ExpiresAt
			if time.Now().Unix() > exp {
				respond.Error(w, r, http.StatusUnauthorized, errors.New("Ошибка авторизации"))
			}

			user, err := s.Store.User().FindById(claims.Id)

			if err != nil {
				respond.Error(w, r, http.StatusUnauthorized, errors.New("Авторизации невозможна"))
				return
			}

			next.ServeHTTP(w, r.WithContext(context.WithValue(r.Context(), "user", user)))
		}
	})
}

func (s *Server) adminAuthorizationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		tokenString := ""
		headerValue := r.Header.Get("Authorization")

		if strings.HasPrefix(headerValue, "Bearer ") && headerValue != "" {
			tokenString = strings.TrimPrefix(headerValue, "Bearer ")
		} else {
			respond.Error(w, r, http.StatusUnauthorized, errors.New("Ошибка авторизации"))
			return
		}

		token, err := jwt.ParseWithClaims(tokenString, &_struct.AdminClaimsType{}, func(token *jwt.Token) (interface{}, error) {
			alg := token.Header["alg"]
			if alg != "HS256" {
				return nil, errors.New("Ошибка авторизации")
			}

			if secretKey, existSecret := os.LookupEnv("ADMIN_SECRET_KEY"); existSecret {
				return []byte(secretKey), nil
			} else {
				return []byte("admin secret key"), nil
			}
		})

		if err != nil {
			respond.Error(w, r, http.StatusUnauthorized, errors.New("Ошибка авторизации"))
			return
		}

		claims, ok := token.Claims.(*_struct.AdminClaimsType)

		if ok && token.Valid {
			exp := claims.ExpiresAt
			if time.Now().Unix() > exp {
				respond.Error(w, r, http.StatusUnauthorized, errors.New("Ошибка авторизации"))
			}

			user, err := s.Store.AdminUser().FindById(claims.Id)

			if err != nil {
				respond.Error(w, r, http.StatusUnauthorized, errors.New("Авторизации невозможна"))
				return
			}

			next.ServeHTTP(w, r.WithContext(context.WithValue(r.Context(), "admin_user", user)))
		}
	})
}

func (s *Server) setRequestId(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := uuid.New().String()
		w.Header().Set("X-Request-ID", id)
		next.ServeHTTP(w, r.WithContext(context.WithValue(r.Context(), ctxKeyRequestId, id)))
	})
}

func (s *Server) getLimit(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		keys := r.URL.Query()

		limit := 10
		offset := 0

		if keys.Get("limit") != "" {
			limit, _ = strconv.Atoi(keys.Get("limit"))

			if limit > 25 || limit < 0 {
				limit = 25
			}
		}

		if keys.Get("offset") != "" {
			offset, _ = strconv.Atoi(keys.Get("offset"))
		}

		r = r.WithContext(context.WithValue(r.Context(), "limit", limit))
		r = r.WithContext(context.WithValue(r.Context(), "offset", offset))

		next.ServeHTTP(w, r)
	})
}

func (s *Server) logRequest(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger := s.logger.WithFields(logrus.Fields{
			"remote_addr": r.RemoteAddr,
			"request_id":  r.Context().Value(ctxKeyRequestId),
		})
		logger.Infof("started %s %s", r.Method, r.RequestURI)

		start := time.Now()
		rw := &responseWriter{w, http.StatusOK}
		next.ServeHTTP(rw, r)

		logger.Infof(
			"completed with %d %s in %v",
			rw.code,
			http.StatusText(rw.code),
			time.Now().Sub(start),
		)
	})
}
