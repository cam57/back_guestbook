package apiserver

import (
	"bitbucket.org/cam57/back_guestbook/internal/app/store/sqlstore"
	"database/sql"
	_ "github.com/lib/pq" // ...
	"net/http"
	"os"
)

func Start(config *Config) error {
	databaseURL := ""
	_, existAppEnv := os.LookupEnv("APP_ENV")
	if !existAppEnv {
		databaseURL = config.DatabaseURL
	} else {

		databaseString, existDatabaseURL := os.LookupEnv("DATABASE_URL")
		if !existDatabaseURL {
			os.Exit(1)
		}
		databaseURL = databaseString
	}

	db, err := newDB(databaseURL)
	if err != nil {
		return err
	}

	defer db.Close()
	store := sqlstore.New(db)
	srv := newServer(store)

	if existAppEnv {
		return http.ListenAndServe(":80", srv)
	}

	return http.ListenAndServe(config.BindAddr, srv)
}

func newDB(dbURL string) (*sql.DB, error) {
	db, err := sql.Open("postgres", dbURL)
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}

	return db, nil
}
