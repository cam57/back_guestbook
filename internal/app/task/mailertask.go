package task

import (
	"bitbucket.org/cam57/back_guestbook/internal/app/constant"
	"bitbucket.org/cam57/back_guestbook/internal/app/model"
	"bitbucket.org/cam57/back_guestbook/internal/app/store"
	"database/sql"
	"encoding/hex"
	"fmt"
	"log"
	"math/rand"
	"net/smtp"
	"time"
)

func Send(user *model.User, store store.Store) {
	if user.EmailConfirmed == constant.Yes {
		return
	}

	nowDate := time.Now().Format(constant.FormatDate)

	ec, err := store.EmailConfirmation().FindByUserId(user.ID, nowDate)
	if err != nil && err != sql.ErrNoRows {
		log.Fatal(err)
	} else if err == sql.ErrNoRows {

		ec.UserId = user.ID
		ec.Expired = time.Now().Add(time.Hour * 24).Format(constant.FormatDate)
		generateToken(ec)

		if err := store.EmailConfirmation().Create(ec); err != nil {
			log.Fatal(err)
		}
	}

	// Connect to the remote SMTP server.
	c, err := smtp.Dial("localhost:1025")
	if err != nil {
		log.Fatal(err)
	}

	// Set the sender and recipient first
	if err := c.Mail("no-replay@tarecon.ru"); err != nil {
		log.Fatal(err)
	}
	if err := c.Rcpt(user.Email); err != nil {
		log.Fatal(err)
	}

	// Send the email body.
	wc, err := c.Data()
	if err != nil {
		log.Fatal(err)
	}
	_, err = fmt.Fprintf(wc, "https://guestbook.ru/"+ec.Token)
	if err != nil {
		log.Fatal(err)
	}
	err = wc.Close()
	if err != nil {
		log.Fatal(err)
	}

	// Send the QUIT command and close the connection.
	err = c.Quit()
	if err != nil {
		log.Fatal(err)
	}
}

func generateToken(ec *model.EmailConfirmation) {
	buff := make([]byte, 24)
	rand.Read(buff)
	str := hex.EncodeToString(buff)

	ec.Token = str[:24]
}
