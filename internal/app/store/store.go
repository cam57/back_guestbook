package store

//Store ...
type Store interface {
	User() UserRepository
	PhoneCode() PhoneCodeRepository
	AdminUser() AdminUserRepository
	EmailConfirmation() EmailConfirmationRepository
	Profile() ProfileRepository
}
