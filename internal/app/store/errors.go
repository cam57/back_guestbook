package store

import "errors"

var (
	ErrRecordNotFound = errors.New("record not found")
	DeviceNotFound    = errors.New("Устойство не найдено")
	BigBossExist      = errors.New("BigBoss exist")
	EmailExist        = errors.New("Пользователь с такой почтой уже существует")
)
