package store

import (
	"bitbucket.org/cam57/back_guestbook/internal/app/model"
	"time"
)

type UserRepository interface {
	Create(*model.User) error
	FindByEmail(string) (*model.User, error)
	FindByPhone(string) (*model.User, error)
	FindById(int) (*model.User, error)
	Update(*model.User) error
	EmailConfirmation(int) error
}

type PhoneCodeRepository interface {
	Create(*model.PhoneCode) error
	FindByPhoneAndCode(string, int, time.Time) (*model.PhoneCode, error)
	FindByPhoneAndDate(string, time.Time) (bool, error)
}

type AdminUserRepository interface {
	Create(*model.AdminUser) error
	FindByEmail(string) (*model.AdminUser, error)
	FindById(int) (*model.AdminUser, error)
}

type EmailConfirmationRepository interface {
	Create(*model.EmailConfirmation) error
	FindByToken(string) (*model.EmailConfirmation, error)
	FindByUserId(int, string) (*model.EmailConfirmation, error)
}

type ProfileRepository interface {
	Create(*model.Profile) error
}
