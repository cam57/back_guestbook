package sqlstore

import (
	"bitbucket.org/cam57/back_guestbook/internal/app/constant"
	//"bitbucket.org/cam57/back_guestbook/internal/app/apiserver"
	"bitbucket.org/cam57/back_guestbook/internal/app/model"
	"database/sql"
	"time"
)

type PhoneCodeRepository struct {
	store *Store
}

// Create ...
func (r *PhoneCodeRepository) Create(pc *model.PhoneCode) error {
	if err := pc.Validate(); err != nil {
		return err
	}

	return r.store.db.QueryRow(
		"INSERT INTO phone_codes (phone, code, expired_date) VALUES ($1, $2, $3) RETURNING id;",
		pc.Phone,
		pc.Code,
		pc.ExpiredDate,
	).Scan(&pc.ID)
}

// FindByPhoneAndCode ...
func (r *PhoneCodeRepository) FindByPhoneAndCode(phone string, code int, date time.Time) (*model.PhoneCode, error) {
	pc := &model.PhoneCode{}
	if err := r.store.db.QueryRow(
		"SELECT id, phone, code FROM phone_codes WHERE phone = $1 AND code = $2  AND expired_date > $3 LIMIT 1;",
		phone,
		code,
		date.Format(constant.FormatDate),
	).Scan(&pc.ID, &pc.Phone, &pc.Code); err != nil {
		//if err == sql.ErrNoRows {
		//	return nil, store.ErrRecordNotFound
		//}

		return nil, err
	}

	return pc, nil
}

// FindByPhoneAndCode ...
func (r *PhoneCodeRepository) FindByPhoneAndDate(phone string, date time.Time) (bool, error) {
	pc := &model.PhoneCode{}
	if err := r.store.db.QueryRow(
		"SELECT id FROM phone_codes WHERE phone = $1 AND expired_date > $2 LIMIT 1;",
		phone,
		date.Format(constant.FormatDate),
	).Scan(&pc.ID); err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}

		return false, err
	}

	return true, nil
}
