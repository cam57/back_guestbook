package sqlstore

import (
	"bitbucket.org/cam57/back_guestbook/internal/app/model"
)

type ProfileRepository struct {
	store *Store
}

func (r *ProfileRepository) Create(profile *model.Profile) error {
	return r.store.db.QueryRow(
		"INSERT INTO profiles (inn, alter_title) VALUES ($1, $2) RETURNING id;",
		profile.Inn,
		profile.AlterTitle,
	).Scan(&profile.Id)
}
