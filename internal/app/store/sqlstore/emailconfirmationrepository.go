package sqlstore

import (
	"bitbucket.org/cam57/back_guestbook/internal/app/constant"
	"bitbucket.org/cam57/back_guestbook/internal/app/model"
	"bitbucket.org/cam57/back_guestbook/internal/app/store"
	"database/sql"
	"time"
)

type EmailConfirmationRepository struct {
	store *Store
}

func (r *EmailConfirmationRepository) Create(ec *model.EmailConfirmation) error {
	return r.store.db.QueryRow(
		"INSERT INTO email_confirmations (user_id, token, expired) VALUES ($1, $2, $3) RETURNING id;",
		ec.UserId,
		ec.Token,
		ec.Expired,
	).Scan(&ec.Id)
}

func (r *EmailConfirmationRepository) FindByToken(token string) (*model.EmailConfirmation, error) {
	ec := &model.EmailConfirmation{}
	if err := r.store.db.QueryRow(
		"SELECT id, user_id, token, expired FROM email_confirmations WHERE token = $1 AND expired > $2;",
		token,
		time.Now().Format(constant.FormatDate),
	).Scan(&ec.Id, &ec.UserId, &ec.Token, &ec.Expired); err != nil {
		if err == sql.ErrNoRows {
			return nil, store.ErrRecordNotFound
		}

		return nil, err
	}

	return ec, nil
}

func (r *EmailConfirmationRepository) FindByUserId(userId int, nowDate string) (*model.EmailConfirmation, error) {
	ec := &model.EmailConfirmation{}
	if err := r.store.db.QueryRow(
		"SELECT id, user_id, token, expired FROM email_confirmations WHERE user_id = $1 AND expired > $2;",
		userId,
		nowDate,
	).Scan(&ec.Id, &ec.UserId, &ec.Token, &ec.Expired); err != nil {

		return ec, err
	}

	return ec, nil
}
