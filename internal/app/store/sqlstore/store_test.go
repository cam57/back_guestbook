package sqlstore_test

import (
	"os"
	"testing"
)

var (
	databaseURL string
)

func TestMain(m *testing.M) {
	databaseURL = os.Getenv("DATABASE_URL")
	if databaseURL == "" {
		databaseURL = "host=main_backend.local dbname=restapi_test user=vagrant password=simplepassword sslmode=disable"
	}

	os.Exit(m.Run())
}
