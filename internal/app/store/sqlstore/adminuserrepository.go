package sqlstore

import (
	"bitbucket.org/cam57/back_guestbook/internal/app/constant"
	"bitbucket.org/cam57/back_guestbook/internal/app/model"
	"bitbucket.org/cam57/back_guestbook/internal/app/store"
	"database/sql"
)

// AdminUserRepository ...
type AdminUserRepository struct {
	store *Store
}

// Create ...
func (r *AdminUserRepository) Create(u *model.AdminUser) error {

	if u.Role == constant.BigBoss {

		countBigBossUser := 0
		if err := r.store.db.QueryRow(
			"SELECT COUNT(*) FROM admin_users WHERE role = $1;",
			u.Role,
		).Scan(&countBigBossUser); err != nil {
			return err
		}

		if countBigBossUser > 0 {
			return store.BigBossExist
		}
	} else {

		countEmailUser := 0
		if err := r.store.db.QueryRow(
			"SELECT COUNT(*) FROM admin_users WHERE email = $1;",
			u.Email,
		).Scan(&countEmailUser); err != nil {
			return err
		}

		if countEmailUser > 0 {
			return store.EmailExist
		}
	}

	if err := u.Validate(); err != nil {
		return err
	}

	if err := u.BeforeCreate(); err != nil {
		return err
	}

	return r.store.db.QueryRow(
		"INSERT INTO admin_users (email, password, full_name, role) VALUES ($1, $2, $3, $4) RETURNING id;",
		u.Email,
		u.Password,
		u.FullName,
		u.Role,
	).Scan(&u.ID)
}

// FindByEmail ...
func (r *AdminUserRepository) FindByEmail(email string) (*model.AdminUser, error) {
	u := &model.AdminUser{}
	if err := r.store.db.QueryRow(
		"SELECT id, email, password, full_name FROM admin_users WHERE email = $1;",
		email,
	).Scan(&u.ID, &u.Email, &u.Password, &u.FullName); err != nil {
		if err == sql.ErrNoRows {
			return nil, store.ErrRecordNotFound
		}

		return nil, err
	}

	return u, nil
}

func (r *AdminUserRepository) FindById(id int) (*model.AdminUser, error) {
	u := &model.AdminUser{}
	if err := r.store.db.QueryRow(
		"SELECT id, email, password, full_name FROM admin_users WHERE id = $1;",
		id,
	).Scan(&u.ID, &u.Email, &u.Password, &u.FullName); err != nil {
		if err == sql.ErrNoRows {
			return nil, store.ErrRecordNotFound
		}

		return nil, err
	}

	return u, nil
}
