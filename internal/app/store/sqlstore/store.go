package sqlstore

import (
	"database/sql"

	"bitbucket.org/cam57/back_guestbook/internal/app/store"
	_ "github.com/lib/pq"
)

//Store ...
type Store struct {
	db                          *sql.DB
	userRepository              *UserRepository
	phoneCodeRepository         *PhoneCodeRepository
	adminUserRepository         *AdminUserRepository
	emailConfirmationRepository *EmailConfirmationRepository
	profileRepository           *ProfileRepository
}

// New ...
func New(db *sql.DB) *Store {
	return &Store{
		db: db,
	}
}

//User ...
func (s *Store) User() store.UserRepository {
	if s.userRepository != nil {
		return s.userRepository
	}

	s.userRepository = &UserRepository{
		store: s,
	}

	return s.userRepository
}

//User ...
func (s *Store) PhoneCode() store.PhoneCodeRepository {
	if s.phoneCodeRepository != nil {
		return s.phoneCodeRepository
	}

	s.phoneCodeRepository = &PhoneCodeRepository{
		store: s,
	}

	return s.phoneCodeRepository
}

func (s *Store) AdminUser() store.AdminUserRepository {
	if s.adminUserRepository != nil {
		return s.adminUserRepository
	}

	s.adminUserRepository = &AdminUserRepository{
		store: s,
	}

	return s.adminUserRepository
}

func (s *Store) EmailConfirmation() store.EmailConfirmationRepository {
	if s.emailConfirmationRepository != nil {
		return s.emailConfirmationRepository
	}

	s.emailConfirmationRepository = &EmailConfirmationRepository{
		store: s,
	}

	return s.emailConfirmationRepository
}

func (s *Store) Profile() store.ProfileRepository {
	if s.profileRepository != nil {
		return s.profileRepository
	}

	s.profileRepository = &ProfileRepository{
		store: s,
	}

	return s.profileRepository
}
