package sqlstore

import (
	"bitbucket.org/cam57/back_guestbook/internal/app/constant"
	"database/sql"

	"bitbucket.org/cam57/back_guestbook/internal/app/model"
	"bitbucket.org/cam57/back_guestbook/internal/app/store"
)

// UserRepository ...
type UserRepository struct {
	store *Store
}

// Create ...
func (r *UserRepository) Create(u *model.User) error {
	if err := u.Validate(); err != nil {
		return err
	}

	if err := u.BeforeCreate(); err != nil {
		return err
	}

	return r.store.db.QueryRow(
		"INSERT INTO users (email, password, phone, full_name, profile_id, role) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id;",
		u.Email,
		u.Password,
		u.Phone,
		u.FullName,
		u.ProfileId,
		u.Role,
	).Scan(&u.ID)
}

// FindByEmail ...
func (r *UserRepository) FindByEmail(email string) (*model.User, error) {
	u := &model.User{}
	if err := r.store.db.QueryRow(
		"SELECT id, email, password, full_name, phone, email_confirmed, profile_id, role FROM users WHERE email = $1;",
		email,
	).Scan(&u.ID, &u.Email, &u.Password, &u.FullName, &u.Phone, &u.EmailConfirmed, &u.ProfileId, &u.Role); err != nil {
		if err == sql.ErrNoRows {
			return nil, store.ErrRecordNotFound
		}

		return nil, err
	}

	return u, nil
}

func (r *UserRepository) FindByPhone(phone string) (*model.User, error) {
	u := &model.User{}
	if err := r.store.db.QueryRow(
		"SELECT id, email, password, full_name, phone, email_confirmed, profile_id, role FROM users WHERE phone = $1;",
		phone,
	).Scan(&u.ID, &u.Email, &u.Password, &u.FullName, &u.Phone, &u.EmailConfirmed, &u.ProfileId, &u.Role); err != nil {
		if err == sql.ErrNoRows {
			return nil, store.ErrRecordNotFound
		}

		return nil, err
	}

	return u, nil
}

func (r *UserRepository) FindById(id int) (*model.User, error) {
	u := &model.User{}
	if err := r.store.db.QueryRow(
		"SELECT id, email, password, phone, full_name, email_confirmed, profile_id, role FROM users WHERE id = $1;",
		id,
	).Scan(&u.ID, &u.Email, &u.Password, &u.Phone, &u.FullName, &u.EmailConfirmed, &u.ProfileId, &u.Role); err != nil {
		if err == sql.ErrNoRows {
			return nil, store.ErrRecordNotFound
		}

		return nil, err
	}

	return u, nil
}

func (r *UserRepository) Update(user *model.User) error {
	if err := user.ValidateUpdate(); err != nil {
		return err
	}

	if err := user.BeforeUpdate(); err != nil {
		return err
	}

	if len(user.Password) > 0 {
		if err := r.store.db.QueryRow(
			"UPDATE users SET password = $1, full_name = $2 WHERE id  = $4 RETURNING id;",
			user.Password,
			user.FullName,
			user.ID,
		).Scan(&user.ID); err != nil {
			return err
		}
	} else {
		if err := r.store.db.QueryRow(
			"UPDATE users SET full_name = $1 WHERE id  = $2 RETURNING id;",
			user.FullName,
			user.ID,
		).Scan(&user.ID); err != nil {
			return err
		}
	}

	return nil
}

func (r *UserRepository) EmailConfirmation(userId int) error {

	if err := r.store.db.QueryRow(
		"UPDATE users SET email_confirmed = $2 WHERE id  = $1 RETURNING id;",
		userId,
		constant.Yes,
	).Scan(&userId); err != nil {
		return err
	}

	return nil
}
