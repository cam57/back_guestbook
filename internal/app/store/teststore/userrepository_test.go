package teststore_test

import (
	"github.com/stretchr/testify/assert"
	"testing"

	"bitbucket.org/cam57/back_guestbook/internal/app/model"
	"bitbucket.org/cam57/back_guestbook/internal/app/store"
	"bitbucket.org/cam57/back_guestbook/internal/app/store/teststore"
)

func TestUserRepository_Create(t *testing.T) {
	s := teststore.New()
	u := model.TestUser(t)
	assert.NoError(t, s.User().Create(u))
	assert.NotNil(t, u)
}

func TestUserRepository_FindByEmail(t *testing.T) {
	s := teststore.New()

	u := model.TestUser(t)

	_, err := s.User().FindByEmail(u.Email)
	assert.EqualError(t, err, store.ErrRecordNotFound.Error())

	s.User().Create(u)
	FindU, err := s.User().FindByEmail(u.Email)
	assert.NoError(t, err)
	assert.NotNil(t, FindU)
}
