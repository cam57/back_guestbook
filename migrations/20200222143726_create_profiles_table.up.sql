create table profiles
(
	id bigserial not null
		constraint profiles_pk
			primary key,
	inn varchar(16),
	alter_title varchar(128)
);