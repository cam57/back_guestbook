create table users
(
	id bigserial not null
		constraint users_pk
			primary key,
	email varchar(128),
	password varchar(256) not null,
	phone varchar(16),
	full_name varchar(64) not null,
	email_confirmed varchar(1) default 'N' not null,
	profile_id bigint not null
		constraint users_profiles_id_fk
			references profiles
				on delete cascade,
	role varchar(16) not null
);