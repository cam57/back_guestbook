create table email_confirmations
(
	id bigserial
		constraint email_confirmations_pk
			primary key,
	user_id bigint not null
		constraint email_confirmations_users_id_fk
			references users
				on delete cascade,
	token varchar(24) not null,
	expired timestamp without time zone not null
);

