create table branches
(
	id bigserial
		constraint branches_pk
			primary key,
	address varchar(256) not null,
	comment varchar(512),
	profile_id bigint not null
);