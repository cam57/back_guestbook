create table services
(
	id bigserial
		constraint services_pk
			primary key,
	title varchar(64) not null,
	default_price numeric(8,2) not null
);

alter table services
	add duration int;