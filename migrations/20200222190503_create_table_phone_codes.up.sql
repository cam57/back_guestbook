create table phone_codes
(
	id bigserial,
	phone varchar(20) not null,
	code smallint,
	expired_date timestamp without time zone not null
);
